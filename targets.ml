
let dns_targets =
  [
    "194.54.14.186";
    "194.54.14.187";
    "194.67.7.1";
    "194.67.2.109";
    "ns5.ntv.ru";
    "ns6.ntv.ru";
    "185.60.135.242"; (* pobeda.aero *)
    "66.81.199.56"; (* same *)
    "87.245.150.33:5353"; (* dme *)
  ]

let orig =
  [
    "https://lenta.ru/";
    "https://ria.ru/";
    "https://ria.ru/lenta/";
    "https://www.rbc.ru/";
    "https://www.rt.com/";
    "http://kremlin.ru/";
    "http://en.kremlin.ru/";
    "https://smotrim.ru/";
    "https://tass.ru/";
    "https://tvzvezda.ru/";
    "https://vsoloviev.ru/";
(*     "https://www.1tv.ru/"; *)
    "https://www.vesti.ru/";
    "https://online.sberbank.ru/";
    "https://sberbank.ru/";
    "https://zakupki.gov.ru/";
(*     "https://www.gosuslugi.ru/"; *)
    "https://er.ru/";
    "https://www.rzd.ru/";
    "https://rzdlog.ru/";
    "https://vgtrk.ru/";
    "https://www.interfax.ru/";
(*     "https://www.mos.ru/uslugi/"; *)
    "http://government.ru/";
(*     "https://mil.ru/"; *)
    "https://www.nalog.gov.ru/";
(*     "https://customs.gov.ru/"; *)
    "https://pfr.gov.ru/";
    "https://rkn.gov.ru/";
(*     "https://www.gazprombank.ru/"; *)
(*     "https://www.vtb.ru/"; *)
    "https://www.gazprom.ru/";
    "https://lukoil.ru";
(*     "https://magnit.ru/"; *)
    "https://www.nornickel.com/";
    "https://www.surgutneftegas.ru/";
(*     "https://www.tatneft.ru/"; *)
    "https://www.evraz.com/ru/";
(*     "https://nlmk.com/"; *)
    "https://www.sibur.ru/";
    "https://www.severstal.com/";
    "https://www.metalloinvest.com/";
    "https://nangs.org/";
    "https://rmk-group.ru/ru/";
    "https://www.tmk-group.ru/";
(*     "https://ya.ru/"; *)
    "https://www.polymetalinternational.com/ru/";
    "https://www.uralkali.com/ru/";
    "https://www.eurosib.ru/";
    "https://ugmk.ua/";
    "https://omk.ru/";
  ]

(* https://the-list.ams3.cdn.digitaloceanspaces.com/t_list.js *)
let t_list = [
"https://lenta.ru/";
"https://auth.ria.ru/";
"https://ria.ru/";
"https://ria.ru/lenta/";
"https://www.rbc.ru/";
"https://www.rt.com/";
"http://kremlin.ru/";
"http://en.kremlin.ru/";
"https://smotrim.ru/";
"https://tass.ru/";
"https://tvzvezda.ru/";
"https://rbc.ru/";
"https://minzdrav.gov.ru/";
"http://government.ru/";
"https://www.mchs.gov.ru/";
"https://rkn.gov.ru/";
"http://www.council.gov.ru/";
"https://www.sberbank.ru/en/individualclients";
"https://kassa.yandex.ru/main-new";
"http://www.gosuslugi.ru/ru/";
"https://www.donland.ru/";
"http://www.fagci.ru/";
"https://digital.gov.ru/ru/";
"https://www.pochta.ru";
"https://www.vbr.ru/goto?externalLink=https://broker.ru/";
"https://www.aeroflot.com/ru-ru";
"https://www.vbr.ru/goto?externalLink=https://alfabank.ru/make-money/investments/";
"https://www.rshb.ru";
"https://www.psbank.ru/";
"https://aversbank.ru/";
(* "https://www.gazprombank.ru"; *)
"https://www.akbars.ru/";
"https://www.aton.ru/";
"https://www.open.ru";
"https://mkb.ru";
"https://www.tinkoff.ru/invest/account/";
"https://www.pochtabank.ru";
"https://www.vbrr.ru/";
"https://er.ru/";
"https://russian.rt.com/";
"https://www.tkbbank.ru/";
"https://wagnera.ru/";
"https://www.tinkoff.ru/";
"https://www.rncb.ru";
"https://www.sberbank.ru/ru/person";
"http://planeradar.ru/virtualradar/desktop.html";
"https://broker.ru/";
"https://bcs.ru/";
"https://im.uralsib.ru/clientendpoint";
]

(* https://the-list.ams3.cdn.digitaloceanspaces.com/transport.js *)
let transport = [
"https://bus.tutu.ru";
"https://busfor.ru/";
"https://www.blablacar.ru/";
"https://www.avtovokzaly.ru/";
"https://bus-69.ru/";
"https://bus.biletyplus.ru/";
"http://78bus.ru/";
"https://severnye-vorota.ru/";
"https://busnovoyas.ru/";
"https://nevskiy-express.ru/";
"https://autobus-moskva.ru/";
"https://ros-bilet.ru/";
"https://xn--d1abb2a.xn--p1ai/";
"https://movibus.ru/";
"https://volgaline34.ru/";
"https://buybusticket.ru/";
"https://donavto.ru/";
"https://rostov-transport.info/";
"https://www.bustime.ru/";
"https://www.intercars-tickets.com";
"https://rov.aero/";
"https://avtovokzal-volgograd.ru/";
"https://www.tourister.ru/";
"https://www.mosgortrans.ru/";
"https://www.avokzal.ru/";
"https://ecolines.net/";
"https://www.autovokzal.org";
"https://www.flixbus.ru/";
"https://tochka-na-karte.ru/";
"https://avtovokzal.gomel.by/";
"https://minsktrans.by/";
"https://kogda.by/";
"https://busfor.by/";
"https://avgrodno.by/";
"https://by.tutu.travel/";
"https://minsk-avtovokzal.by/";
"https://ticketbus.by/";
"https://taf.by/";
"https://gomel-minsk.by/";
"https://tickets.by/";
"https://star-bus.ru/";
"https://kaktustour.com/";
"https://618.by/";
"http://busik24.com/";
"https://2y.by/";
"https://travel-kassa.ru/";
"https://av.brest.by/";
"https://avtovokzal-mogilev.by/";
"https://ecolines.by/";
"https://www.mts.by";
"https://donavto.ru/";
"https://rostov-transport.info/";
"https://www.bustime.ru/";
"https://rov.aero/";
"https://avtovokzal-volgograd.ru/";
"https://ros-bilet.ru/";
"https://rosukrbus.com/";
"https://www.moscowbus.ru/";
  ]

let by = [
  "http://belta.by/";
  "https://sputnik.by/";
  "https://www.tvr.by/";
  "https://www.sb.by/";
  "https://belmarket.by/";
  "https://www.belarus.by/";
  "https://belarus24.by/";
  "https://ont.by/";
  "https://www.024.by/";
  "https://www.belnovosti.by/";
  "https://mogilevnews.by/";
  "https://www.mil.by/";
  "https://yandex.by/";
  "https://www.slonves.by/";
  "http://www.ctv.by/";
  "https://radiobelarus.by/";
  "https://radiusfm.by/";
  "https://alfaradio.by/";
  "https://radiomir.by/";
  "https://radiostalica.by/";
  "https://radiobrestfm.by/";
  "https://www.tvrmogilev.by/";
  "https://minsknews.by/";
  "https://zarya.by/";
  "https://grodnonews.by/";
  "https://rec.gov.by/ru";
  "https://www.mil.by/";
  "http://www.government.by/";
  "https://president.gov.by/ru";
  "https://www.mvd.gov.by/ru";
  "http://www.kgb.by/ru/";
  "http://www.prokuratura.gov.by/";
  "http://www.nbrb.by/";
   "https://belarusbank.by/";
   "https://brrb.by/";
   "https://www.belapb.by/";
   "https://bankdabrabyt.by/";
   "https://belinvestbank.by/individual";
 "https://bgp.by/ru/";
 "https://www.belneftekhim.by";
 "http://www.bellegprom.by";
 "https://www.energo.by";
 "http://belres.by/ru/";
]

let crypto = [
  "https://cleanbtcru/";
  "https://bonkypay.com/";
  "https://changer.club/";
  "https://superchange.net";
  "https://mine.exchange/";
  "https://platov.co/";
  "https://ww-pay.net/";
  "https://delets.cash/";
  "https://betatransfer.org/";
  "https://ramon.money/";
  "https://coinpaymaster.com/";
  "https://bitokk.biz/";
  "https://www.netex24.net/";
  "https://cashbank.pro/";
  "https://flashobmen.com/";
  "https://abcobmen.com/";
  "https://ychanger.net/";
  "https://multichange.net/";
  "https://24paybank.ne/";
  "https://royal.cash/";
  "https://prostocash.com/";
  "https://baksman.org/";
  "https://kupibit.me/";
  "https://abcobmen.com/";
]

let other = Devkit.Action.config_lines_exn "targets.txt"

let targets = (orig @ t_list @ transport @ by @ crypto @ other) |> List.sort_uniq String.compare |> Array.of_list
