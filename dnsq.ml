

open Printf
open Devkit
open ExtLib
open Lwt_unix

let log = Log.from "dnsq"

let () = Random.self_init ()

let verbose = ref 0
let maxlen = 1024

type dres =
  | DNS of Dns.rcode (* rcode of DNS query answer *)
  | TIMEOUT (* query performed, but no answer received *)
  | ADDR (* no query was needed, address converted from string *)
  | INVALID (* no query was needed, domain name invalid *)
  | BLACKDOMAIN (* no query was needed, domain name is blacklisted *)
  | BLACKIP (* all domain ips are blacklisted *)

let show_dres = function
  | DNS rcode -> Dns.string_of_rcode rcode
  | TIMEOUT -> "TIMEOUT"
  | ADDR -> "ADDR"
  | INVALID -> "INVALID"
  | BLACKDOMAIN -> "BLACKDOMAIN"
  | BLACKIP -> "BLACKIP"

type ttl = Forever | Seconds of int
type ip = { ip : Network.ipv4; ttl : ttl; }
type result = { domain: string; dres: dres; cname: string option; ips: ip list; txt : string list list }

type t = {
  sock : file_descr;
  addr : sockaddr;
  timeout : float;
  buf : bytes;
  edns : int option;
  h : (int * string, result Lwt.u) Hashtbl.t Lazy.t;
  mutable queries : int;
}

let upstream_addr ?(timeout=3.) ?edns addr =
  let%lwt proto = getprotobyname "udp" in
  let sock = socket PF_INET SOCK_DGRAM proto.p_proto in
  setsockopt_float sock SO_RCVTIMEO timeout;
  setsockopt_float sock SO_SNDTIMEO timeout;
  let%lwt () = connect sock addr in
  Lwt_unix.set_close_on_exec sock;
  Lwt.return { sock; addr; timeout; buf = Bytes.create (Option.default maxlen edns); edns; h = lazy (Hashtbl.create 13); queries = 0; }

let upstream ?(port=53) ?edns ?timeout host =
  upstream_addr ?edns (Nix.make_inet_addr_exn host port) ?timeout

let get_reply_exn { sock; addr; buf; _ } =
  let%lwt msg =
    let%lwt (len,peer) = recvfrom sock buf 0 (Bytes.length buf) [] in
    if peer = addr then
      Lwt.return @@ Bytes.sub_string buf 0 len
    else
      Exn_lwt.fail "Wrong peer : expected %s got %s" (Nix.show_addr addr) (Nix.show_addr peer)
  in
  if !verbose > 2 then
    print_endline @@ Action.hexdump msg;
  if !verbose > 1 then
    print_endline @@ Dns.pkt_out_s @@ Dns.to_pkt msg;
  let (info,typ,cname,addrs,txt) = Dns.parse msg in
  match typ, info with
  | `Reply (rcode,_aa,_ra), { Dns.id; qtype = Dns.A | TXT | MX; domain; } ->
    Lwt.return (id, Inet.string_of_domain domain, rcode, Option.map Inet.string_of_domain cname, addrs, txt)
  | _ ->
    Exn_lwt.fail "expected reply to A or TXT query"

let resolve_immediate domain =
  match Network.ipv4_of_string_exn domain with
  | addr -> Some { domain; dres = ADDR; cname = None; ips = [{ ip = addr; ttl = Forever; }]; txt = [] } (* do not query IP address *)
  | exception _ -> None
(*
    match Inet.is_dns_domain domain with
    | true -> None
    | false -> Some { domain; dres = INVALID; cname = None; ips = []; txt = [] }
*)

let send_query srv dns_id qtype domain =
  let pkt = Bytes.unsafe_of_string @@ Dns.make_query ?edns:srv.edns dns_id qtype domain in
  srv.queries <- srv.queries + 1;
  let%lwt len = sendto srv.sock pkt 0 (Bytes.length pkt) [] srv.addr in
  if len = Bytes.length pkt then
    Lwt.return ()
  else
    Exn_lwt.fail "can't send full packet for %S to %s" domain (Nix.show_addr srv.addr)

let query_exn srv ?(qtype=Dns.A) domain =
  match resolve_immediate domain with
  | Some x -> x
  | None ->
  let dns_id = Random.int (succ Dns.max_id) in
  let t = new Action.timer in
  Lwt_main.run (send_query srv dns_id qtype domain);
  let rec loop () =
    let (id',domain,rcode,cname,addrs,txt) = Lwt_main.run (get_reply_exn srv) in
    if dns_id <> id' then
    begin
      if t#get > srv.timeout then Exn.fail "timeouted";
      loop ()
    end
    else
      let ips = List.map (fun (ip, ttl) -> { ip; ttl = Seconds (Int32.to_int ttl); }) addrs in
      { domain; dres = DNS rcode; cname; ips; txt }
  in
  loop ()

(* asynchronous interface *)

let running t = if Lazy.is_val t.h then Hashtbl.length !!(t.h) else 0

let setup srv =
  match Lazy.is_val srv.h with
  | true -> Lazy.force srv.h
  | false ->
    let h = Lazy.force srv.h in
    let rec loop () =
      match%lwt Exn_lwt.map get_reply_exn srv with
      | `Exn Lwt.Canceled -> Lwt.(fail Canceled)
      | `Exn exn -> log #warn ~exn "receive loop %s" (Nix.show_addr srv.addr); loop ()
      | `Ok (id,domain,rcode,cname,addrs,txt) ->
          match Hashtbl.find_option h (id,domain) with
          | None -> (*log #debug "orphan reply : %d %s" id domain (* timeouted? *) *) loop ()
          | Some u ->
            Hashtbl.remove_all h (id,domain);
            let ips = List.map (fun (ip, ttl) -> { ip; ttl = Seconds (Int32.to_int ttl); }) addrs in
            Lwt.wakeup u { domain; dres = DNS rcode; cname; ips; txt };
            loop ()
    in
    Lwt.ignore_result (loop ()); (* run loop *)
    h

let status t = sprintf "dns %s running %d done %d" (Nix.show_addr t.addr) (running t) t.queries

let send_query_forget t ?(qtype=Dns.A) domain =
  let dns_id = Random.int (succ Dns.max_id) in
  send_query t dns_id qtype domain

let do_real_query t qtype domain =
  let h = setup t in
  let dns_id = Random.int (succ Dns.max_id) in
  let req = (dns_id,domain) in
  if Hashtbl.mem h req then log #warn "(%d,%s) query queued already (use cache!)" dns_id domain;
  let (result,u) = Lwt.task () in
  Hashtbl.replace h req u;
  let%lwt () = send_query t dns_id qtype domain in
  try%lwt
    Lwt.pick [Lwt_unix.timeout t.timeout; result]
  with
    Lwt_unix.Timeout | Lwt.Canceled -> Hashtbl.remove_all h req; Lwt.return { domain; dres = TIMEOUT; cname = None; ips = []; txt = [] }

let do_query t ?(qtype=Dns.A) domain =
  match resolve_immediate domain with
  | Some x -> Lwt.return x
  | None -> do_real_query t qtype domain

(*
let refill t =
  try
    while running t < t.rate do
      let (s,cb) = Queue.peek t.source in
      Enum.iter (fun domain -> do_query t domain cb; if running t >= t.rate then raise Exit) e;
      ignore (Queue.pop t.source) (* exhausted this enum, drop it *)
    done
  with
  | Exit | Queue.Empty -> (*log #info "%s" (status t);*) ()
  | exn -> log #warn ~exn "refill"
*)

(*
let query t domain k =
    Queue.push (domain, k) t.source;
  refill t
*)

(*
let stop t =
  log #debug "stop %s" (Nix.show_addr t.addr);
  if not !(t.stop) then
  begin t.stop := true; Ev.del t.ev; Ev.del t.timer; Hashtbl.clear t.h end
*)
