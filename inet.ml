
open Devkit
open ExtLib
open Printf

let uint_of_ipv4 = sprintf "%lu" $ Network.int32_of_ipv4
let raw_of_ipv4 = Int32.to_string $ Network.int32_of_ipv4
let ipv4_of_raw = Network.ipv4_of_int32 $ Int32.of_string

type domain = string list

let string_of_domain = String.concat "."
let domain_of_string s = Stre.nsplitc s '.'

let domain_equal d1 d2 =
  try
    List.for_all2 (fun n1 n2 -> String.uppercase n1 = String.uppercase n2) d1 d2
  with
  | Invalid_argument _ -> false

(* www.site.com site.com -> www *)
let subdomain_prefix d1 d2 =
  if String.ends_with d1 d2 then
    match String.length d1 - String.length d2 with
    | 0 -> ""
    | n -> if d1.[n - 1] = '.' then String.slice ~last:(n-1) d1 else ""
  else
    ""

(* d1 subdomain of d2 *)
let is_subdomain d1 d2 =
  if String.ends_with d1 d2 then
    match String.length d1 - String.length d2 with
    | 0 -> true
    | n -> d1.[n - 1] = '.'
  else
    false

(* empty labels are prevented by Html_lexer *)
let check_dns_labels =
  let rec loop s i =
    match String.index_from s i '.' with
    | exception Not_found -> String.length s - i <= 63
    | j -> j - i <= 63 && loop s (j+1)
  in
  fun s -> loop s 0

let check_valid_dns s = String.length s <= 255 && check_dns_labels s

(*
(** keep old version for Domain *)
let is_dns_domain_old host = Html_lexer.is_dns_domain (Lexing.from_string host)

(* should be used on punycode *)
(* TODO unify in one ragel machine *)
let is_dns_domain host = is_dns_domain_old host && check_valid_dns host
let is_internet_hostname host = Html_lexer.is_internet_hostname (Lexing.from_string host) && check_valid_dns host && Root_domain.has_valid_tld host
*)

let cidr_compare cidr ip =
  if Network.ipv4_matches ip cidr then 0 else compare (Network.prefix_of_cidr cidr) ip

let cidr_equal cidr1 cidr2 = Network.(ipv4_matches (prefix_of_cidr cidr1) cidr2)
