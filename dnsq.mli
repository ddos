(** Simple DNS stub resolver. *)

(**
All domains must be punycoded.
*)

open Devkit

val verbose : int ref

(** Type of a DNS server result code. *)
type dres =
  | DNS of Dns.rcode (** rcode of DNS query answer *)
  | TIMEOUT (** query performed, but no answer received *)
  | ADDR (** no query was needed, address converted from string *)
  | INVALID (** no query was needed, domain name invalid *)
  | BLACKDOMAIN (** no query was needed, domain name is blacklisted *)
  | BLACKIP (** all domain ips are blacklisted *)

val show_dres : dres -> string

(** Type for the result of a DNS query. *)
type ttl = Forever | Seconds of int
type ip = { ip : Network.ipv4; ttl : ttl; }
type result = { domain: string; dres: dres; cname: string option; ips: ip list; txt : string list list }

(** Type of a handle to perform DNS queries. Contains the sockaddr where to send queries packet on, amongst other info. *)
type t

(** [upstream ?port ?edns ?timeout endpoint] creates a handle to send DNS queries to hostname [endpoint].
  NB this function creates a socket which is never closed. I.e. call it once per upstream in the process lifetime,
  e.g. at toplevel (as a lazy value).
  API should be improved to allow destroying values of type [t] *)
val upstream : ?port:int -> ?edns:int -> ?timeout:float -> string -> t Lwt.t

(** [status t] display status such as the number of queries running and number of queries done for the handler [t]. *)
val status : t -> string

(** [query_exn t domain] performs a synchronous query with [t] on [domain].
  @param qtype query type A (default) or TXT
  *)
val query_exn : t -> ?qtype:Dns.qtype -> string -> result

(** [query_exn t domain] performs an asynchronous query ([qtype] A or TXT) with [t] on [domain].
  @param qtype query type A (default) or TXT
*)
val do_query : t -> ?qtype:Dns.qtype -> string -> result Lwt.t

val send_query_forget : t -> ?qtype:Dns.qtype -> string -> unit Lwt.t
