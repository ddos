open Printf

include Targets

let target_domains = targets |> Array.map (fun url ->
  match Devkit.Stre.nsplitc url '/' with
  | domain::[] -> domain
  | _scheme::""::domain::_ -> domain
  | _ -> failwith @@ sprintf "bad target %S" url)

let agents =
  [|
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36";
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36";
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0";
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36";
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36";
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36";
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36";
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15";
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15";
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0";
  |]

type t = {
  ok : int;
  errors : int;
}

let random a = a.(Random.int (Array.length a))

let setup h =
  let open Curl in
  set_useragent h (random agents);
  set_nosignal h true;
  set_dnscachetimeout h 120;
  set_connecttimeoutms h 3000;
  set_timeoutms h 3000;
  set_followlocation h true;
  set_sslverifypeer h false;  (* ssl check must be turned off when using http_proxy, to permit https page caching*)
  set_sslverifyhost h SSLVERIFYHOST_NONE;
  set_cainfo h "/dev/null";
(*   set_sslcipherlist h "DEFAULT@SECLEVEL=1"; *)
(*   set_maxredirs h 1; *)
  set_ipresolve h IPRESOLVE_V4;
  set_encoding h CURL_ENCODING_ANY;
(*   set_freshconnect h true; (* more connections *) *)
(*   set_forbidreuse h true; *)
  set_protocols h [CURLPROTO_HTTP; CURLPROTO_HTTPS;];
  set_redirprotocols h [CURLPROTO_HTTP; CURLPROTO_HTTPS;];
(*   set_maxrecvspeedlarge h (Int64.of_int (kBps*1024)); *)
  ()

let () = Printexc.register_printer (function Curl.CurlException (code,n,s) -> Some (sprintf "%d %s %s" n s (Curl.strerror code)) | _ -> None)

let main ~par =
  Random.self_init ();
  Devkit.Nix.raise_limits ();
  let stat = Hashtbl.create 10 in
  let dns_stat = Hashtbl.create 10 in
  targets |> Array.iter (fun s -> Hashtbl.add stat s { ok = 0; errors = 0 });
  dns_targets |> List.iter (fun s -> Hashtbl.add dns_stat s 0);
  let worker h =
    while%lwt true do
      let url = random targets in
      let open Curl in
      reset h;
      setup h;
      set_url h (if Random.int 3 = 0 then url else sprintf "%s?%d" url (Random.int 2000));
      set_writefunction h String.length;
      try%lwt
        let%lwt r = Curl_lwt.perform h in
        if r <> CURLE_OK then failwith @@ sprintf "curl error %s" (Curl.strerror r);
        Hashtbl.replace stat url
          (let t = Hashtbl.find stat url in
           { t with ok = t.ok + 1 }
          );
        Lwt.return_unit
      with _exn ->
(*         print_endline @@ sprintf "FAILED: %s : %s" (Curl.get_effectiveurl h) (Printexc.to_string exn); *)
        Hashtbl.replace stat url
          (let t = Hashtbl.find stat url in
           { t with errors = t.errors + 1 }
          );
        Lwt.return_unit
    done
  in
  let qtypes = Dns.[| A ; NS ; CNAME ; SOA ; MX ; TXT ; AAAA ; A6 ; PTR |] in
  let dns_worker upstreams =
    while%lwt true do
      let (upstream,ns) = random upstreams in
      Hashtbl.replace dns_stat upstream (Hashtbl.find dns_stat upstream + 1);
      let domain = random target_domains in
      let qtype = random qtypes in
      let%lwt () = Dnsq.send_query_forget ns ~qtype domain in
      Lwt_unix.sleep @@ 0.5 +. Random.float 1.
    done
  in
  let show_stats () =
    while%lwt true do
      let%lwt () = Lwt_unix.sleep 1. in
      print_endline "dns";
      dns_targets |> List.iter (fun s -> print_endline @@ sprintf "%32s %10d" s (Hashtbl.find dns_stat s));
      print_endline @@ sprintf "%40s %6s %6s %40s %6s %6s %40s %6s %6s" "url" "ok" "errors" "url" "ok" "errors" "url" "ok" "errors";
      targets |> Array.iteri (fun i url ->
        let { ok; errors } = Hashtbl.find stat url in
        printf "%40s %6d %6d" url ok errors;
        if i mod 3 = 2 then print_newline ()
      );
      print_newline ();
      Lwt.return_unit
    done
  in
  Lwt_engine.set (new Lwt_engine.libev ());
  List.iter (fun n -> Sys.set_signal n Sys.Signal_ignore) [Sys.sigtstp; Sys.sigttou; Sys.sigttin; Sys.sighup; Sys.sigpipe];
  let (_:int list) = Unix.sigprocmask SIG_BLOCK [Sys.sigtstp; Sys.sigttou; Sys.sigttin; Sys.sighup; Sys.sigpipe] in
  Lwt_main.run @@ begin
    let workers = List.init par (fun _ -> Curl.init ()) |> List.map worker in
    let%lwt dns_upstreams = dns_targets |> Lwt_list.map_s (fun s ->
      let (host,port) =
        match Devkit.Stre.nsplitc s ':' with
        | [host;port] -> host, int_of_string port
        | [s] -> s, 53
        | _ -> failwith @@ sprintf "bad dns %s" s
      in
      let%lwt ns = Dnsq.upstream ~timeout:2. ~port host in
      Lwt.return (s,ns)
    ) |> Lwt.map Array.of_list in
    let dns_workers = List.init par (fun _ -> dns_worker dns_upstreams) in
    Lwt.join (show_stats () :: workers @ dns_workers)
  end;
  print_endline "exit"

let () =
  try
    let par =
      match Devkit.Nix.args with
      | [n] -> int_of_string n
      | _ -> 1000
    in
    main ~par
  with exn ->
    print_endline @@ sprintf "fatal exn %s : %s" (Printexc.to_string exn) (Printexc.get_backtrace ());
